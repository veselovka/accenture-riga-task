package com.accenture

object Task extends App {

  // Give meaningful names, improve code, write some tests.

  def a(b: List[AnyRef]): List[String] =
    b.foldLeft(List[String]())((c, d) => d match {
      case null => c
      case e: String => c :+ e
      case f: List[AnyRef] => c ++ a(f)
    })

  /**
   * ex-a function
   */
  def flatFold(list: List[AnyRef]): List[String] =
    list flatMap {
      case str: String ⇒ List(str)
      case l: List[_] ⇒ flatFold(l collect { case x: AnyRef ⇒ x })
      case _ ⇒ List.empty // or throw exception if this is unacceptable
    }

  def g(h: List[String]) =
    h.foldLeft(List[String]())((i, j) => i.lastOption match {
      case Some(`j`) => i
      case _ => i :+ j
    })

  /**
   * ex-g function
   */
  def dropRepeated(list: List[String]) =
    list.foldLeft(List.empty[String]) {
      case (acc@(_ :+ last), elem) if elem == last ⇒ acc
      case (acc, elem) ⇒ acc :+ elem
    }
}
