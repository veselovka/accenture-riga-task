package com.accenture

import org.scalatest.FunSuite
import com.accenture.Task._

class Test extends FunSuite {
  test("dropRepeated") {
    val list1 = List.empty[String]
    val res1 = dropRepeated(list1)
    val originalRes1 = g(list1)
    assert(originalRes1 === res1)
    assert(res1.isEmpty)

    val list2 = List("a", "a", "b", "c", "a", "d", "d", "d")
    val res2 = dropRepeated(list2)
    val originalRes2 = g(list2)
    assert(originalRes2 === res2)
    val expected2 = List("a", "b", "c", "a", "d")
    assert(res2 === expected2)
  }

  test("flatFold") {
    val list1 = List.empty[String]
    val res1 = flatFold(list1)
    val originalRes1 = a(list1)
    assert(originalRes1 === res1)
    assert(res1.isEmpty)

    val list2 = List("a", null, List("b", "c", null, "d"))
    val res2 = flatFold(list2)
    val originalRes2 = a(list2)
    assert(originalRes2 === res2)
    assert(res2 === List("a", "b", "c", "d"))

    val list3 = List("a", List(1, "b"))
    intercept[MatchError] {
      a(list3)
    }
    val res3 = flatFold(list3)
    assert(res3 === List("a", "b"))
  }
}
